\documentclass[]{article}
\usepackage{geometry}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{setspace}
\usepackage{amsmath}
\usepackage{longtable}
\usepackage{floatrow}
\usepackage{cases}
\usepackage{bm}
\usepackage{textcomp}
\usepackage{esint}
\usepackage[bookmarks,
                     colorlinks=true,
                     linkcolor=red,
                     urlcolor=blue,
                     citecolor=gray]{hyper ref}

\newcommand{\ket}[1]{\left| #1 \right>} % for Dirac bras
\newcommand{\bra}[1]{\left< #1 \right|} % for Dirac kets
\newcommand{\braket}[2]{\left< #1 \vphantom{#2} \right|
 \left. #2 \vphantom{#1} \right>} % for Dirac brackets
\begin{document}
\title{Title}
\author{Author}
\date{Today}
\maketitle

\section{Reflectivity and Transmissivity of Bragg Mirrors}
We use the characteristic matrix methods described in Principles of Optics by Born \& Wolf.\\
At normal incidence, the characteristic matrix across the layers that compose the alternating mirror are:
\begin{equation}
M = M_aM_b ~~~ M_a=\begin{pmatrix} \cos k_aa & -\frac{i}{n_a}\sin k_aa \\ -in_a\sin k_aa & \cos k_aa \end{pmatrix} ~~~ M_b=\begin{pmatrix} \cos k_bb & -\frac{i}{n_b}\sin k_bb \\ -in_b\sin k_bb & \cos k_bb \end{pmatrix}
\end{equation}
where $k_a=(\omega/c)n_a$ and $k_b=(\omega/c)n_b$.\\
At oblique incidence, the characteristic matrix is different for TE and TM modes. For TE mode we have
\begin{equation}
M_a = \begin{pmatrix} \cos(k_aa\cos\theta_a) & -\frac{i}{p}\sin(k_aa\cos\theta_a) \\ -ip\sin(k_aa\cos\theta_a) & \cos(k_aa\cos\theta_a) \end{pmatrix} ~~~ p = n_a\cos\theta_a
\end{equation}
for TM mode, we just replace $p$ with $q$: $p\rightarrow q=\cos_a\theta/n_a$.\\
Also the formula for reflect and transmission coefficient are as follows:
\begin{equation}
\begin{split}
& r=\frac{(m_{11}+m_{12}p_f)p_0-(m_{21}+m_{22}p_f)}{(m_{11}+m_{12}p_f)p_0+(m_{21}+m_{22}p_f)}\\
& t = \frac{2p_0}{(m_{11}+m_{12}p_f)p_0+(m_{21}+m_{22}p_f)}
\end{split}
\end{equation}
where $p_0=n_0\cos\theta_0$, $p_f=n_f\cos\theta_f$. The reflectivity and transmissivity are
\begin{equation}
\mathcal{R}=|r|^2 ~~~~~~ \mathcal{T}=\frac{p_f}{p_0}|t|^2
\end{equation}
as usual, the above results are for TE mode, for TM mode, we just replace $p$ with $q$.\\
~\\
We first look at the normal incidence case. We use quarter-wave films:
\begin{equation}
n_aa=n_bb=\lambda_0/4
\end{equation}
if we use the light with wavelength $\lambda_0$, then the characteristic matrix becomes
\begin{equation}
M=\begin{pmatrix}-\frac{n_b}{n_a} & 0 \\ 0 & -\frac{n_a}{n_b}  \end{pmatrix} \Rightarrow M_N=\begin{pmatrix}\Big{[}-\frac{n_b}{n_a}\Big{]}^N & 0 \\ 0 & \Big{[}-\frac{n_a}{n_b}\Big{]}^N \end{pmatrix}
\end{equation}
and the reflect and transmission coefficient are
\begin{equation}
r =\frac{1-\frac{n_f}{n_0}(\frac{n_A}{n_B})^{2N}}{1+\frac{n_f}{n_0}(\frac{n_A}{n_B})^{2N}}   ~~~~~~ t = \frac{2(-\frac{n_A}{n_B})^N}{1+\frac{n_f}{n_0}(\frac{n_A}{n_B})^{2N}}
\end{equation}

\section{Normal Incidence with Varying Wavelength}

The characteristic matrix is
\begin{equation}
M =\begin{pmatrix}\cos(\frac{\pi}{2}\frac{\lambda_0}{\lambda}) & -\frac{i}{n_a}\sin(\frac{\pi}{2}\frac{\lambda_0}{\lambda})\\ -in_a\sin(\frac{\pi}{2}\frac{\lambda_0}{\lambda}) & \cos(\frac{\pi}{2}\frac{\lambda_0}{\lambda})\end{pmatrix}\begin{pmatrix}\cos(\frac{\pi}{2}\frac{\lambda_0}{\lambda}) & -\frac{i}{n_b}\sin(\frac{\pi}{2}\frac{\lambda_0}{\lambda})\\ -in_b\sin(\frac{\pi}{2}\frac{\lambda_0}{\lambda}) & \cos(\frac{\pi}{2}\frac{\lambda_0}{\lambda})\end{pmatrix}
\end{equation}
Define the small parameter $x$ and keep only linear terms in $x$:
\begin{equation}
x=\frac{\pi}{2}\frac{\lambda-\lambda_0}{\lambda_0} ~~~ M=\begin{pmatrix}-\frac{n_b}{n_a} &i(\frac{1}{n_a}+\frac{1}{n_b})x \\ i(n_a+n_b)x & -\frac{n_a}{n_b}  \end{pmatrix}
\end{equation}
the eigenvalue of matrix $M$ is:
\begin{equation}
[-\frac{n_b}{n_a}-\lambda][-\frac{n_a}{n_b}-\lambda]+(n_a+n_b)(\frac{1}{n_a}+\frac{1}{n_b})x^2=0
\end{equation}
\begin{equation}
\lambda_{\pm}=\frac{-(\frac{n_b}{n_a}+\frac{n_a}{n_b})\pm(\frac{n_b}{n_a}-\frac{n_a}{n_b})}{2}\mp\frac{(n_a+n_b)(\frac{1}{n_a}+\frac{1}{n_b})}{\frac{n_b}{n_a}-\frac{n_a}{n_b}}x^2
\end{equation}
\begin{equation}
x=\frac{\pi}{2}\frac{\lambda-\lambda_0}{\lambda_0}=-\frac{\pi}{2}\frac{\omega-\omega_0}{\omega_0}
\end{equation}
%\section{Question One}
%If I accept the following polariton theory:
%\begin{equation}
%\begin{split}
%& E_{cav}=\frac{\hbar c}{n_c}\sqrt{k^2_{\perp}+k^2_{\parallel}}\approx E_{cav}(k_{\parallel}=0)+\frac{\hbar^2k^2_{\parallel}}{2m_{cav}} ~~~~~~ k_{\perp}=\frac{2\pi}{\lambda_0}\\
%& E_{LP}(k_{\parallel})=\frac{1}{2}[E_{exc}(k_{\parallel})+E_{cav}(k_{\parallel})-\sqrt{4g^2+(E_{exc}-E_{cav})^2}]
%\end{split}
%\end{equation}
%in which they think only the light with $k_{\perp}=\frac{2\pi}{\lambda_0}$ can reach the cavity due to resonant tunneling. Then I think about the incident angle dependence. For incoherent excitation, different incident angle corresponds to different wave-vector but with the same normal component $k_{\perp}=\frac{2\pi}{\lambda_0}$. Look at the characteristic matrix (2), it's clear that
%\begin{equation}
%k_aa\cos\theta_a=k_{\perp}n_aa
%\end{equation}

\section{Oblique Incidence with Varying Angles}
From the numerics we know that in the stop band the reflectivity is rather close to unity, also the stop band is well suited to the approximation that the angle is small. Thus we can use the small angle approximation to see the life-time difference between TE and TM mode.\\
~\\
First of all, we have Snell's law for the relations between angles:
\begin{equation}
n_0\sin\theta_0=n_a\sin\theta_a=n_b\sin\theta_b ~~~\Rightarrow ~~~ n_0\theta_0=n_a\theta_a=n_b\theta_b
\end{equation}
Secondly, in terms of varying angles, the characteristic matrix for TE mode has the following form:
\begin{equation}
M_a=\begin{pmatrix} \cos\frac{\pi}{2}\frac{\cos\theta_a}{\cos\theta_0} & \frac{-i}{n_a\cos\theta_a}\sin\frac{\pi}{2}\frac{\cos\theta_a}{\cos\theta_0} \\ -in_a\cos\theta_a\sin\frac{\pi}{2}\frac{\cos\theta_a}{\cos\theta_0} & \cos\frac{\pi}{2}\frac{\cos\theta_a}{\cos\theta_0}\end{pmatrix}
\end{equation}
In the small angle approximation and using the Snell's law we have
\begin{equation}
M_a\approx A_a+B_a\theta^2_0
\end{equation}
where
\begin{equation}
A_a = \begin{pmatrix} 0 & -\frac{i}{n_a} \\ -in_a & 0\end{pmatrix} ~~~~~~ B_a = \begin{pmatrix} \frac{\pi}{4}[(\frac{n_0}{n_a})^2-1] & -\frac{i}{n_a}\frac{1}{2}(\frac{n_0}{n_a})^2 \\ in_a\frac{1}{2}(\frac{n_0}{n_a})^2 & \frac{\pi}{4}[(\frac{n_0}{n_a})^2-1] \end{pmatrix}
\end{equation}
Then the characteristic matrix for one periodic layer is
\begin{equation}
M = M_aM_b = (A_aA_b)+(A_aB_b+B_aA_b)\theta^2_0
\end{equation}
then for N periodic layers we have
\begin{equation}
M_N=(A_aA_b)^N+U\theta^2_0
\end{equation}
Matrix $U$ is calculable because $(A_aA_b)$ is diagonal, which can be seen from the following:
\begin{equation}
\begin{split}
& A_aA_b=\begin{pmatrix} s & 0 \\ 0 & t \end{pmatrix} ~~~~~~ (A_aB_b+B_aA_b)=\begin{pmatrix} a & b \\ c & d \end{pmatrix}\\
& U = (A_aA_b)^{N-1}(A_aB_b+B_aA_b) + (A_aA_b)^{N-2}(A_aB_b+B_aA_b)(A_aA_b)+\cdots + (A_aB_b+B_aA_b)(A_aA_b)^{N-1}\\
& U_{11}=Ns^{N-1}a ~~~ U_{22}=Nt^{N-1}d\\
& U_{12}=(s^{N-1}+s^{N-2}t+\cdots+t^{N-1})b=s^{N-1}\frac{1-(t/s)^N}{1-(t/s)}b\\
& U_{21}=(t^{N-1}+t^{N-2}s+\cdots+s^{N-1})b=s^{N-1}\frac{1-(t/s)^N}{1-(t/s)}c
\end{split}
\end{equation}
here we have
\begin{equation}
s = -\frac{n_b}{n_a} ~~~ t=-\frac{n_a}{n_b}
\end{equation}
\begin{equation}
\begin{split}
& a =\frac{n_b}{2n_a}[(\frac{n_0}{n_b})^2-(\frac{n_0}{n_a})^2]\\
& b= -i\frac{\pi}{4}\{\frac{1}{n_b}[(\frac{n_0}{n_a})^2-1]+\frac{1}{n_a}[(\frac{n_0}{n_b})^2-1] \} \\ 
& c= -i\frac{\pi}{4}\{n_b[(\frac{n_0}{n_a})^2-1]+n_a[(\frac{n_0}{n_b})^2-1] \}\\
& d= \frac{n_a}{2n_b}[(\frac{n_0}{n_a})^2-(\frac{n_0}{n_b})^2]
\end{split}
\end{equation}
~\\
~\\
For TM mode, matrix $A_a$ and $B_a$ are changed to
\begin{equation}
A_a = \begin{pmatrix} 0 & -in_a \\ -\frac{i}{n_a} & 0\end{pmatrix} ~~~~~~ B_a = \begin{pmatrix} \frac{\pi}{4}[(\frac{n_0}{n_a})^2-1] & -in_a\frac{1}{2}(\frac{n_0}{n_a})^2 \\ \frac{i}{n_a}\frac{1}{2}(\frac{n_0}{n_a})^2 & \frac{\pi}{4}[(\frac{n_0}{n_a})^2-1] \end{pmatrix}
\end{equation}
then the resulting parameters are:
\begin{equation}
\begin{split}
& s = -\frac{n_a}{n_b} ~~~ t=-\frac{n_b}{n_a}\\
& a =\frac{n_a}{2n_b}[(\frac{n_0}{n_b})^2-(\frac{n_0}{n_a})^2]\\
& b= -i\frac{\pi}{4}\{n_b[(\frac{n_0}{n_a})^2-1]+n_a[(\frac{n_0}{n_b})^2-1] \}  \\ 
& c= -i\frac{\pi}{4}\{\frac{1}{n_b}[(\frac{n_0}{n_a})^2-1]+\frac{1}{n_a}[(\frac{n_0}{n_b})^2-1] \}\\
& d= \frac{n_b}{2n_a}[(\frac{n_0}{n_a})^2-(\frac{n_0}{n_b})^2]
\end{split}
\end{equation}
~\\
~\\
we assume that $n_a<n_b$, $N\gg1$ and $n_0=n_f=1$ to simplify the resulting reflective coefficient:
\begin{equation}
\begin{split}
& r_{TE} = \frac{s^N(1-\frac{1}{2}\theta_0^2)+(U_{11}+U_{12}-U_{21})\theta^2_0}{s^N(1-\frac{1}{2}\theta_0^2)+(U_{11}+U_{12}+U_{21})\theta^2_0}\\
& r_{TM} = \frac{-t^N(1-\frac{1}{2}\theta_0^2)+(U_{12}-U_{21}-U_{22})\theta^2_0}{-t^N(1-\frac{1}{2}\theta_0^2)+(U_{12}+U_{21}+U_{22})  \theta^2_0}
\end{split}
\end{equation}
note that $s$ in TE mode equals $t$ in TM mode. It's easy to see that all exponential dependence on number of layers will cancer because each term inside the expression has the same exponential dependence (for TE mode each term $\propto s^{N-1}$, for TM mode, each term $\propto t^{N-1}$), thus the difference between TE mode and TM mode is small, not exponential in number of layers.\\
~\\
Actually this is consistent with previous rough result. Previously when I thought there was the exponential dependence, it's actually the following form:
\begin{equation}
\frac{\gamma_{TE}}{\gamma_{TM}}\propto (\frac{\cos\theta_a}{\cos\theta_b})^{2N}=(\frac{1-1/2\theta^2_a}{1-1/2\theta^2_b})^{2N}=[1-\frac{1}{2}(\theta^2_a-\theta^2_b)]^{2N}=[1-\frac{1}{2}(\frac{n_0^2}{n^2_a}-\frac{n_0^2}{n^2_b})\theta^2_0]^{2N}
\end{equation}
Previously I thought there is exponential dependence because the term inside the brackets is smaller than one. But if the result is kept to the leading term in small angle $\theta_0$, the pre-factor has no exponential dependence on $N$.

\end{document}