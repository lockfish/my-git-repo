\documentclass[]{article}
\usepackage{geometry}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{setspace}
\usepackage{amsmath}
\usepackage{longtable}
\usepackage{floatrow}
\usepackage{cases}
\usepackage{bm}
\usepackage{textcomp}
\usepackage{esint}
\usepackage[bookmarks,
                     colorlinks=true,
                     linkcolor=red,
                     urlcolor=blue,
                     citecolor=gray]{hyper ref}

\newcommand{\ket}[1]{\left| #1 \right>} % for Dirac bras
\newcommand{\bra}[1]{\left< #1 \right|} % for Dirac kets
\newcommand{\braket}[2]{\left< #1 \vphantom{#2} \right|
 \left. #2 \vphantom{#1} \right>} % for Dirac brackets

\newcommand{\rnot}{\reflectbox{\ensuremath\neg}}

\begin{document}

\title{Quantum Few-Body Problem}
\date{\today}
\maketitle
\tableofcontents

\section{General References}

\begin{itemize}
\item Collision Theory by Marvin L. Goldberger and Kenneth M. Watson

\item Elementary Particle Physics and Scattering Theory (Two Volumes, Brandeis University Summer Institute in Theoretical Physics, 1967),  edited by M. Chretien and S. S. Schweber

\item Mathematical Aspects of the Three-Body Problem in the Quantum Scattering Theory by L. D. Faddeev

\item Quantum Scattering Theory for Several Particle Systems by L. D. Faddeev and S. P. Merkuriev
\end{itemize}
Several concepts and notable points:
\begin{itemize}
\item A note on convention: the scattering length defined in Landau series and in the Feshbach resonance literature differ by sign.

\item Resonance: enhancement of regular scattering or decay loss due to the presence of a nearby bound state (or discrete level, serving as an extra channel). There is resonance at low energies, resonance at quasi-discrete levels, resonances between different channels.

\item Unitary Limit: upper bound for the scattering cross section; in terms of phase shift, it reaches the upper bound which is $\pi/2$, and then emerges form below as $-\pi/2$ (the $\pi$ jump), then it continues its growing without breaking the unitarity.

\item Zero-Range Models:
\begin{itemize}
\item The short range details and even the characteristic range of interaction do not appear explicitly in the experssion of the transition matrix or equivalently of the scattering amplitude for a low energy process. That is why it appears natural to model interaction by using a formal zero-range potential.

\item On delta source leads to a purely isotropic scattering process (s-wave scattering). Contact condition: Wigner-Bethe-Peierls, where the interaction is replaced with a zero-range source.
\end{itemize}

\item Separable Potential: \href{http://journals.aps.org/pr/abstract/10.1103/PhysRev.139.B691}{Separable Potentials and Coulomb Interaction} by David R. Harrington (1965)

\item Skorniakov Ter-Martirosian equation: an integral equation for the source amplitude. 
\begin{itemize}
\item Zh. Eksp. Teor. Phys. 31, 775(1956) [Sov. Phys. JETP 4, 648 (1957)] by G. V. Skorniakov and K. A. Ter-Martirosian 

\item Sov. Phys. JETP 13, 349 (1961) by G. Danilov : Skorniakov Ter-Martirosian equation is ill defined for three interacting bosons and so on.
\end{itemize}
\end{itemize}

\section{Bound State Problems}

\begin{itemize}
\item \href{http://scitation.aip.org/content/aapt/journal/ajp/55/1/10.1119/1.14961}{Bound states in weak attractive potentials in one-dimensional quantum mechanics} by J. B. Bronzan (1987)

\item \href{http://www.sciencedirect.com/science/article/pii/0003491676900385}{The bound state of weakly coupled Schrödinger operators in one and two dimensions} by Barry Simon (1976)

\item \href{http://www.math.uiuc.edu/~dirk/preprints/simonfest8.pdf}{Some Bound State Problems in Quantum Mechanics} by Dirk Hundertmrak
\end{itemize}

\section{Quantum Three-Body Problem}

\subsection{Hyperspherical Approach}

\begin{itemize}
\item \href{http://pubs.acs.org/doi/abs/10.1021/j100112a048}{Selected applications of hyperspherical harmonics in quantum theory} by John Avery

\item \href{http://link.springer.com/article/10.1007%2FBF02738575}{Hyperspherical Functions and Quantum-Mechanical Three-Body Problem} by H.Letz

\item \href{http://journals.aps.org/pra/abstract/10.1103/PhysRevA.82.022706}{Green's Functions and the Adiabatic Hyperspherical Method} by Seth T. Rittenhouse, N. P. Mehta, and Chris H. Greene

\item \href{http://scitation.aip.org/content/aip/journal/jmp/47/6/10.1063/1.2203430}{Closed form of the generalized Green’s function for the Helmholtz operator on the two-dimensional unit sphere} by Radoslaw Szmytkowski

\item \href{http://iopscience.iop.org/article/10.1088/1751-8113/40/5/009/pdf}{Closed form of the generalized Green’s function for the Helmholtz operator on the N-dimensional unit sphere} by Radoslaw Szmytkowski

\item \href{http://journals.aps.org/pra/pdf/10.1103/PhysRevA.56.83}{Derivation of planar three-body hyperspherical harmonics from monopole harmonics} by Kevin A. Mitchell and Robert G. Littlejohn

\item \href{http://arxiv.org/pdf/physics/9905051.pdf}{Quantum Three Body Problem} by Zhong-Qi Ma and An-Ying Dai

\item \href{http://www.sciencedirect.com/science/article/pii/037015739400094J}{Hyperspherical coordinate approach to atomic and other Coulombic three-body systems} by C. D. Lin

\item \href{https://jila.colorado.edu/sites/default/files/assets/files/publications/phdthesis_jiawang.pdf}{Hyperspherical Approach to Quantal Three-Body} by Jia Wang (Ph. D thesis)

\item \href{http://link.springer.com/article/10.1007/s006010050102}{Hyperspherical-Harmonics Methods for Few-Body Problems} by R. Krivec
\end{itemize}

\subsection{Efimov Physics}
The Efimov effect is an effect in the quantum mechanics of few-body systems, referring to a scenario in which three identical bosons interact, with the prediction that there exist bound states (called Efimov states) of three bosons even if the two-particle attraction is too weak to allow two bosons to form a pair. A three-particle Efimov state, where the two-body sub-system is unbounded, is often depicted symbolically by the Borromean rings, which means that if one of the particles is removed, the remaining two fall apart. In this case the Efimov state is also called a Borromean state. Atomically speaking, the Efimov effect is what happens when two atoms that normally repel each other become strongly attracted when a third atom is introduced. The unusual Efimov state has an infinite number of similar states, they are completely identical except their sizes and energy levels scale by a universal factor of approximately 22.7 (in the case of three identical bosonic particles).\\
~\\
Efimov physics is a kind of universality close to resonance in cold atomic gas. The Efimov states are independent of the underlying interaction and can in principle be observed in all quantum mechanical systems (molecular, atomic and nuclear). The size of each three-particle Efimov state is much larger than the force-range between individual particle pairs. There are only two parameters involved in trimmer states: the low-energy scattering length and the three-body parameter. The role of the three-body parameter is just to determine where exactly the series of Efimov states begins.\\
~\\
A related concept is the halo states in atomic nuclei. In nuclear physics, an atomic nucleus is called a halo nucleus or is said to have a nuclear halo when it has a core nucleus surrounded by a halo of orbiting protons or neutrons, which makes the radius of the nucleus appreciably larger than that predicted by the liquid drop model.\\
~\\
Efimov states not only exist for three identical bosons, distinguishable particles can be involved as well as particles of different masses (the mass ratio changes the geometric scaling factor), and under certain conditions even particle system involving two identical fermions can exhibit Efimov states.\\
~\\
No true Efimov effect exists for four or more identical particles, here true meaning: a precise scaling invariance and well defined thresholds (an $N-1$ bound state should cause the appearance of an infinite number of $N$-body bound states). But other universality exists for four-body physics: each Efimov state is accompanied by a pair of universal tetramer states. A related concept is the Tjon lines in nuclear physics.\\
~\\
Related Literature:
\begin{itemize}
\item \href{https://www.uibk.ac.at/exphys/ultracold/projects/levt/efimov/SovJNucPhys12.589.efimov.pdf}{Weakly-Bound States of Three Resonantly-Interacting Particles} by V. N. Efimov (1970)

\item \href{http://www.sciencedirect.com/science/article/pii/0370269370903497}{Energy Levels Arising from Resonant Two-Body Forces in a Three-Body System} by V. N. Efimov (1970)

\item  \href{http://www.sciencedirect.com/science/article/pii/0370269375903780}{Bound states of $^4He$ with local interactions} by A. J. Tjon (1975)

\item \href{http://journals.aps.org/prc/abstract/10.1103/PhysRevC.20.340}{Tjon line in few-body systems} by R. Perne and H. Kroger (1979).

\item \href{http://www.nature.com/nphys/journal/v5/n8/pdf/nphys1355.pdf}{Giant Trimers True to Scale} by V. N. Efimov (2009)

\item \href{http://journals.aps.org/prd/abstract/10.1103/PhysRevD.5.1992}{Efimov's Effect: A New Pathology of Three-Particle Systems. II} by R. D. Amado and J. V. Noble (1972)

\item \href{https://physics.aps.org/articles/v3/9}{Forty Years of Efimov Physics} (2010)

\item \href{http://arxiv.org/abs/1001.1981}{Efimov States in Nuclear and Particle Physics} by Hammer and Platter

\item \href{http://rsta.royalsocietypublishing.org/content/roypta/369/1946/2679.full.pdf}{Efimov physics from a renormalization group perspective} by Hammer and Platter

\item \href{http://arxiv.org/abs/0812.0528}{Efimov effect from functional renormalization} by S. Moroz, S. Floerchinger, R. Schmidt \& C. Wetterich (2008)

\item \href{http://arxiv.org/abs/1102.0896}{Efimov physics from the functional renormalization group} by Stefan Floerchinger, Sergej Moroz \&  Richard Schmidt (2011)

\item \href{http://arxiv.org/abs/1409.3294}{Onset of the Limit Cycle and Universal Three-Body Parameter in Efimov Physics} by Yusuke Horinouchi \& Masahito Ueda (2014)

\item \href{http://arxiv.org/abs/1603.05328}{Topological origin of universal few-body clusters in Efimov physics} by Yusuke Horinouchi \& Masahito Ueda (2016)

\item \href{http://journals.aps.org/prd/abstract/10.1103/PhysRevD.7.2517}{There Is No Efimov Effect for Four or More Particles} by R. D. Amado and F. C. Greenwood.

\item \href{http://arxiv.org/abs/1210.5147}{Why there is no Efimov effect for four bosons and related results on the finiteness of the discrete spectrum} by Dmitry K. Gridnev

\item \href{http://www.nature.com/nphys/journal/v9/n2/full/nphys2523.html}{Efimov Effect in Quantum Magnets} by Yusuke Nishida, Yasuyuki Kato and Cristian D. Batista
\end{itemize}

\subsection{Universality and Scaling}
\begin{itemize}
\item \href{http://journals.aps.org/prd/abstract/10.1103/PhysRevD.8.1195}{Low-Energy Behavior of the Few-Body Scattering Amplitudes} by Sadhan K. Adhikari (1973)

\item \href{http://www.sciencedirect.com/science/article/pii/S0375947498006502}{The three-boson system with short-range interactions} by P.F. Bedaque, H.-W. Hammer, U. van Kolck (1999). Renormalization, effective field theory, non-perturbative. - Three-body interaction apart from two-body interactions.

\item \href{http://journals.aps.org/prl/abstract/10.1103/PhysRevLett.85.908}{Three-body Recombination in Bose Gases with Large Scattering Length} by P. F. Bedaque, Eric Braaten, and H.-W. Hammer (2000). Three-Body recombination constant is not universal.

\item \href{http://www.sciencedirect.com/science/article/pii/S0370157306000822}{Universality in Few-Body Systems with Large Scattering Length} by H. W. Hammer (2006)

\item \href{http://arxiv.org/abs/0908.0852}{Universality in Ultra-Cold Few- and Many-Boson Systems} by Martin Thogersen (Ph. D thesis)

\item \href{http://arxiv.org/abs/nucl-th/0507060}{Few-body physics in effective field theory} by Hammer

\item \href{http://journals.aps.org/prl/abstract/10.1103/PhysRevLett.107.120401}{Universality of the Three-Body Parameter for Efimov States in Ultracold Cesium} by M. Berninger $et.al.$
\end{itemize}

\subsection{Other Approaches}
\begin{itemize}
\item G. V. Skorniakov and K. A. Ter-Martirosian, Zh. Eksp. Teor. Fiz. 31, 775 (1956) and Sov. Phys. JETP 4, 648 (1957). This is the first solution to the dimer-atom scattering, which gives the integral equation for the low energy scattering amplitude.

\item \href{http://www.slac.stanford.edu/cgi-wrap/getdoc/slac-r-079.pdf}{Faddeev Equations for Local Potentials} by T. Osborn (Ph. D thesis about Faddeev-Yakubovskii-Type Integral Equations)

\item \href{http://journals.aps.org/pra/abstract/10.1103/PhysRevA.72.060702}{Differential form of the Skornyakov–Ter-Martirosyan Equations} by F. M. Pen’kov and W. Sandhas (2005). Zero-range model for three-body systems using Skornyakov Ter-Martirosyan equation has drawback: the Hamiltonian is not self-adjoint. Danilov tried to correct it, but it cannot be used in numerics. An extension of zero-range model beyond STM equation is used to derive the Efimov physics (adiabatic expansion in configuration space). Infinite number of equations, cut at some number N.

\item \href{http://journals.aps.org/pr/abstract/10.1103/PhysRev.115.1643}{Quantum-Mechanical Three-Body Problem} by Leonard Eyges (1959)

\item \href{http://journals.aps.org/pr/abstract/10.1103/PhysRev.121.1744}{Quantum Mechanical Three-Body Problem. II} by Leonard Eyges (1961)

\item \href{http://jetp.ac.ru/cgi-bin/dn/e_012_05_1014.pdf}{Scattering Theory for a Three-Particle System} by L. D . Faddeev (1961)

\item \href{http://www.jetp.ac.ru/cgi-bin/e/index/e/13/2/p349?a=list}{On the Three-Body Problem with Short-Range Forces} by G.S. Danilov (1961)

\item \href{http://www.jetp.ac.ru/cgi-bin/dn/e_014_06_1315.pdf}{Comments on the problem of three particles with point interactions} by R. Minlos and L. D. Faddeev (1961)

\item \href{http://journals.aps.org/prl/abstract/10.1103/PhysRevLett.19.473}{Kohn Variational Principle for Three-Particle Scattering} by Nuttall (1967)

\item \href{http://journals.aps.org/prl/abstract/10.1103/PhysRevLett.100.140404}{Analytical Solution of the Bosonic Three-Body Problem} by Alexander O. Gogolin $et.al.$

\item \href{http://ptp.oxfordjournals.org/content/52/1/131.full.pdf+html}{Theory of Scattering in Three-Particle System} by Masahiko Hirooka and Sigenobu Sunakawa

\item \href{http://ptp.oxfordjournals.org/content/54/1/79.full.pdf}{Theory of Scattering in Three-Particle System and its Application to Electron-Exciton Scattering} by Kiyohisa Matsuda, Masahiko Hirooka and Sigenobu Sunakawa
\end{itemize}

\section{Few-Body Physics in Lower Dimensions}
\begin{itemize}
\item \href{https://journals.aps.org/pra/abstract/10.1103/PhysRevA.3.1133}{Binding of Two Closed-Shell Atoms on a Solid Surface} by Amitabha Bagchi (1971)

\item \href{http://scitation.aip.org/content/aip/journal/jcp/70/10/10.1063/1.437251}{Trimer binding in two dimensions} by F. Cabral and L. W. Bruch (1979)

\item \href{http://journals.aps.org/pra/abstract/10.1103/PhysRevA.22.28}{Near-threshold behavior of the ground-state binding energies of the few-atom systems of $^4He$ and other bosons in two and three dimensions} by T. K. Lim et. al. (1980) 

\item \href{http://journals.aps.org/pra/abstract/10.1103/PhysRevA.19.425}{Binding of three identical bosons in two dimensions} by L. W. Bruch and J. A. Tjon (1979), two s-wave bound states: $B_3=16.5 B_2$ and $B_3=1.27B_2$.

\item \href{http://journals.aps.org/pra/abstract/10.1103/PhysRevA.64.012706}{Interatomic collisions in a tightly confined Bose gas} by D. S. Petrov and G. V. Shlyapnikov (2001)

\item \href{http://arxiv.org/abs/1107.3017}{Scaling and universality in two dimensions: three-body bound states with short-ranged interactions} by F. F. Bellotti et. al. (2011)

\item \href{http://journals.aps.org/pra/abstract/10.1103/PhysRevA.85.025601}{A supercirlce description of universal three-body states in two dimensions} by F. F. Bellotti (2012)

\item \href{http://arxiv.org/abs/1211.6342v2}{Mass-imbalanced Three-Body Systems in Two Dimensions} by F. F. Bellotti $et.al.$ (2013)

\item \href{http://arxiv.org/abs/1408.3981}{Three-particle Complexes in Two-Dimensional Semiconductors} by Bogdan Ganchev, Neil Drummond, Igor Aleiner \& Vladimir Fal'ko (2015)

\item \href{http://journals.aps.org/prl/abstract/10.1103/PhysRevLett.115.180406}{Generalized Efimov Effect in One Dimension} by Sergej Moroz, José P. D’Incao, and Dmitry S. Petrov (2015)

\item \href{http://arxiv.org/abs/1511.05489}{Hyperspherical approach to the three-bosons problem in 2D with a magnetic field} by Seth T. Rittenhouse, Andrew Wray, B. L. Johnson (2015)

\item \href{http://iopscience.iop.org/article/10.1088/1367-2630/18/4/043023/meta;jsessionid=5627DBFD5FE1368ADE9CAC3C76D91CD2.c4.iopscience.cld.iop.org}{Three-body bound states of two bosonic impurities immersed in a Fermi sea in 2D} (2016). Three-particle problem with external media.
\end{itemize}

\section{BEC-BCS, Feshbach Resonance and Fermionic Problems}

\begin{itemize}
\item \href{http://journals.aps.org/pra/abstract/10.1103/PhysRevA.67.010703}{Three-body problem in Fermi gases with short-range interparticle interaction} by D. S. Petrov (2003). Three-body processes in ultra-cold two-component Fermi gases with short-range interaction characterized by a large and positive scattering length in 3D - two identical fermions interacting with a third particle - two universality classes.

\item \href{http://journals.aps.org/prl/abstract/10.1103/PhysRevLett.93.143201}{Three-Boson Problem near a Narrow Feshbach Resonance} by D. S. Petrov (2004)

\item \href{http://journals.aps.org/prl/abstract/10.1103/PhysRevLett.93.090404}{Weakly Bound Dimers of Fermionic Atoms} by D. S. Petrov, C. Salomon and G. V. Shlyapnikov (2004)

\item \href{http://journals.aps.org/pra/pdf/10.1103/PhysRevA.71.012708}{Scattering properties of weakly bound dimers of fermionic atoms} by D. S. Petrov, C. Salomon, and G. V. Shlyapnikov (2005)

\item \href{http://journals.aps.org/pra/abstract/10.1103/PhysRevA.73.032724}{Exact diagrammatic approach for dimer-dimer scattering and bound states of three and four resonantly interacting particles} by I. V. Brodsky et. al. (2005). Diagrammatic approach to STM-type integral equation. Bound state is found by divergence of the scattering amplitude. Numerical integration, kernel inversion and diagonalization. Result in 2D: bbf case, repulsive interaction not taken into account - parameter free result. No disappearance of three-particle bound state. Only scattering length is relevant, while $r_0$ should also be relevant. Inclusion of literatures that are important.

\item \href{http://journals.aps.org/pra/abstract/10.1103/PhysRevA.73.053607}{Properties of strongly paired fermionic condensates} by J. Levinsen and V. Gurarie (2006) About BEC-BCS crossover and gap equation. Both scattering length and effective range $r_0$ are relevant. Numerical integration using Gauss-Legendre quadrature, integration in infinite space.

\item \href{http://journals.aps.org/prl/abstract/10.1103/PhysRevLett.99.090402}{Spectrum and Dynamics of the BCS-BEC Crossover from a Few-Body Perspective} by J. von Stecher and Chris H. Greence (2007)

\item \href{http://journals.aps.org/prl/abstract/10.1103/PhysRevLett.99.130407}{Crystalline Phase of Strongly Interacting Fermi Mixtures} by D. S. Petrov et. al. (2007) 

\item \href{http://journals.aps.org/pra/abstract/10.1103/PhysRevA.77.032707}{Collisional properties of weakly bound heteronuclear dimers} by B. Marcelis, S. J. J. M. F. Kokkelmans, G. V. Shlyapnikov, and D. S. Petrov (2008). About Efimov in cold atom systems.

\item \href{http://journals.aps.org/pra/abstract/10.1103/PhysRevA.81.043634}{Dimer-atom scattering between two identical fermions and a third particle} by M. Iskin (2010). In correspondence to Petrov 2003. Diagrammatic T-matrix approach to three-body scattering problem between two identical fermions and a third particle - unequal mass and fall of a particle to the center - the Efimov physics. Momentum space. Numerical integration, discretization using Gaussian-Legendre quadrature method, integral equaiton reduces to a matrix-eigenvalue problem, calculation of the scattering length. No calculation of binding energy.

\item \href{http://journals.aps.org/pra/abstract/10.1103/PhysRevA.82.062706}{Atom-dimer scattering length for fermions with different masses: Analytical study of limiting cases} by F. Alzetto, R. Combescot, and X. Leyronas, the unequal-mass generalization of Skorniakov-Ter-Martirosian equations.

\item \href{http://journals.aps.org/pra/abstract/10.1103/PhysRevA.92.022704}{Three-body recombination in heteronuclear mixtures at finite temperature} by D. S. Petrov and F. Werner (2015)
\end{itemize} 

\section{The Book: Elementary Particle Physics and Scattering Theory}
\subsection{Contents}
\begin{itemize}
\item Superconvergence, Current Algebra, and Sum Rules
\item High-Energy Behavior of Scattering Amplitudes
\item Particles and Sources
\item Singularities in the Complex Angular Momentum Plane
\item Current Algebra
\item The Three-Body Problem
\item Topics in Scattering Theory
\item Coherence Correlation and Quantum Field Theory
\item Analyticity Properties and Bounds of the Scattering Amplitudes
\end{itemize}


\section{New Section}
This section is created to examplify the use of git as a version control system,so it has no actual meaning.
\end{document}
