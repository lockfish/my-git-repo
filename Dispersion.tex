\documentclass[]{article}
\usepackage{geometry}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{setspace}
\usepackage{amsmath}
\usepackage{longtable}
\usepackage{floatrow}
\usepackage{cases}
\usepackage{bm}
\usepackage{textcomp}
\usepackage{esint}
\usepackage[bookmarks,
                     colorlinks=true,
                     linkcolor=red,
                     urlcolor=blue,
                     citecolor=gray]{hyper ref}

\newcommand{\ket}[1]{\left| #1 \right>} % for Dirac bras
\newcommand{\bra}[1]{\left< #1 \right|} % for Dirac kets
\newcommand{\braket}[2]{\left< #1 \vphantom{#2} \right|
 \left. #2 \vphantom{#1} \right>} % for Dirac brackets
\begin{document}

\section{Bragg Mirror}
According to the previous calculation, we are concerned with the following approximate characteristic matrix:\\
For TE mode
\begin{equation}
M_N=M^N ~~~ M=\begin{pmatrix} -\frac{n_b\cos\theta_b}{n_a\cos\theta_a} & i(\frac{1}{n_a}+\frac{1}{n_b})x \\ i(n_a+n_b)x & -\frac{n_a\cos\theta_a}{n_b\cos\theta_b}\end{pmatrix}
\end{equation}
\begin{equation}
M=A+iBx ~~~ A=\begin{pmatrix} -\frac{n_b\cos\theta_b}{n_a\cos\theta_a} &0 \\0 & -\frac{n_a\cos\theta_a}{n_b\cos\theta_b}\end{pmatrix} ~~~ B=\begin{pmatrix} 0 & \frac{1}{n_a}+\frac{1}{n_b} \\ n_a+n_b & 0\end{pmatrix}
\end{equation}
\begin{equation}
M_N=A^N+iUx ~~~ A^N=\begin{pmatrix} (-\frac{n_b\cos\theta_b}{n_a\cos\theta_a})^N & 0 \\ 0 & (-\frac{n_a\cos\theta_a}{n_b\cos\theta_b})^N\end{pmatrix}
\end{equation}
\begin{equation}
U=s^{N-1}\frac{1-(t/s)^N}{1-(t/s)}\begin{pmatrix} 0 & u=\frac{1}{n_a}+\frac{1}{n_b} \\ v=n_a+n_b & 0\end{pmatrix} ~~~ s=-\frac{n_b\cos\theta_b}{n_a\cos\theta_a} ~~~ t=-\frac{n_a\cos\theta_a}{n_b\cos\theta_b}
\end{equation}
\begin{equation}
M^N_{11}=s^N ~~~ M^N_{12}=s^{N-1}\frac{1-(t/s)^N}{1-(t/s)}(iux) ~~~ M^N_{21}=s^{N-1}\frac{1-(t/s)^N}{1-(t/s)}(ivx) ~~~ M^N_{22}=t^N
\end{equation}
The formula for transmission coefficient is as follows, where $p=n\cos\theta$:
\begin{equation}
r=\frac{(m_{11}+m_{12}p_f)p_0-(m_{21}+m_{22}p_f)}{(m_{11}+m_{12}p_f)p_0+(m_{21}+m_{22}p_f)}
\end{equation}
Plug in the matrix elements of $M_N$, we get the following expression:
\begin{equation}
r_{TE}=\frac{1-\frac{n_f\cos\theta_f}{n_0\cos\theta_0}(\frac{t}{s})^N}{1+\frac{n_f\cos\theta_f}{n_0\cos\theta_0}(\frac{t}{s})^N}\Big{\{} 1+(ix)s^{N-1}\frac{1-(t/s)^N}{1-(t/s)}[\frac{un_f\cos\theta_fn_0\cos\theta_0-v}{s^Nn_0\cos\theta_0-t^Nn_f\cos\theta_f}-\frac{un_f\cos\theta_fn_0\cos\theta_0+v}{s^Nn_0\cos\theta_0+t^Nn_f\cos\theta_f}] \Big{\}}
\end{equation}
Assuming $n_a<n_b$, it's reasonable to make the following approximation to make the expression simple:
\begin{equation}
\begin{split}
r_{TE}&=\Big{(}1-2\frac{n_f\cos\theta_f}{n_0\cos\theta_0}(\frac{t}{s})^N \Big{)}\exp\Big{[}-ix\frac{2v}{(s-t)n_0\cos\theta_0}\Big{]}\\
&=\Big{[}1-2\frac{n_f\cos\theta_f}{n_0\cos\theta_0}\Big{(}\frac{n_a\cos\theta_a}{n_b\cos\theta_b}\Big{)}^{2N} \Big{]}\exp\Big{[}\frac{i\pi(n_a+n_b)}{(\frac{n_b\cos\theta_b}{n_a\cos\theta_a}-\frac{n_a\cos\theta_a}{n_b\cos\theta_b})\omega_0n_0\cos\theta_0}(\omega-\omega_0)\Big{]}
\end{split}
\end{equation}
Similarly we have the following expression for TM mode:
\begin{equation}
r_{TM}=\Big{[}1-2\frac{n_f\cos\theta_0}{n_0\cos\theta_f}\Big{(}\frac{n_a\cos\theta_b}{n_b\cos\theta_a}\Big{)}^{2N} \Big{]}\exp\Big{[}\frac{i\pi(n_a+n_b)\cos\theta_0}{(\frac{n_b\cos\theta_a}{n_a\cos\theta_b}-\frac{n_a\cos\theta_b}{n_b\cos\theta_a})\omega_0n_0}(\omega-\omega_0)\Big{]}
\end{equation}

\section{Quantum Well and Cavity}
The reflective and transmission coefficient are as follows:\\
For TE mode
\begin{equation}
r_{TE}(\omega)=\frac{i\Gamma_0/\cos\theta}{\omega_{ex}-\omega-i(\Gamma_0/\cos\theta+\gamma)} ~~~ t_{TE}(\omega)=1+r_{TE}
\end{equation}
where $\Gamma_0$ is the exciton radiative broadening and $\gamma$ is the homogeneous broadening of the exciton resonance caused by acoustic phonons.\\
For TM mode
\begin{equation}
r_{TM}=\frac{i\Gamma_0\cos\theta}{\omega_{ex}-\omega-i(\Gamma_0\cos\theta+\gamma)} ~~~ t_{TM}=1+r_{TM}
\end{equation}
The transfer matrix for quantum well has the following expression:
\begin{equation}
T_{QW}=\frac{1}{t}\begin{pmatrix} t^2-r^2 & r \\ -r & 1 \end{pmatrix}
\end{equation}
The transfer matrix for free propagation in the cavity:
\begin{equation}
T=\begin{pmatrix} e^{ik_zL_c/2} & 0 \\ 0 & e^{-ik_zL_c/2} \end{pmatrix} ~~~ k_z=\frac{\omega}{c}\cos\theta ~~~ L_c=\lambda_0=\frac{2\pi c}{\omega_0\cos\theta}
\end{equation}

\section{Dispersion of Cavity Polariton}
Consider the symmetric structure, the eigen-modes correspond to the poles of transmission coefficient of the whole structure. Equivalently it corresponds to the non-trivial solutions of Maxwell's equations under the requirement of no light incident on the cavity from outside. This means the following:
\begin{equation}
T_c\begin{pmatrix} r_B \\ 1 \end{pmatrix}=A\begin{pmatrix} 1 \\ r_B \end{pmatrix}
\end{equation}
where $T_c$ is the transfer matrix for the cavity and quantum well only, without Bragg mirrors. And $r_B$ is the reflective coefficient of the Bragg mirrors. Eliminating factor $A$ we get the equation:
\begin{equation}
\frac{T^c_{21}r_B+T^c_{22}}{T^c_{11}r_B+T^c_{12}}=r_B \Rightarrow [r_B(2r_{QW}+1)e^{ik_zL_c}-1][r_Be^{ik_zL_c}+1]=0
\end{equation}
The polariton dispersion should be obtained from the first brackets because it contains quantum well parameters.
\begin{equation}
r_B=r\exp[i\alpha(\omega-\omega_0)]=r[1+i\alpha(\omega-\omega_0)] ~~~ e^{ik_zL_c}=1+i\frac{2\pi}{\omega_0}(\omega-\omega_0)
\end{equation}
\begin{equation}
\begin{split}
& (\omega-\omega_{ex}+i\gamma)(\omega-\omega_0+i\gamma_c)=V^2\\
 \Rightarrow &~\omega_{\pm}=\frac{\omega_0+\omega_{ex}}{2}-\frac{i}{2}(\gamma+\gamma_c)\pm\sqrt{(\frac{\omega_{ex}-\omega_{0}}{2})^2+V^2-(\frac{\gamma-\gamma_c}{2})^2+\frac{i}{2}(\omega_{ex}-\omega_0)(\gamma_c-\gamma)}
\end{split}
\end{equation}
~\\
where for TE mode
\begin{equation}
\gamma_c=\frac{1-r_{TE}}{r_{TE}}\frac{1}{\alpha_{TE}+\frac{2\pi}{\omega_0}}=2\frac{n_f\cos\theta_f}{n_0\cos\theta_0}\Big{(}\frac{n_a\cos\theta_a}{n_b\cos\theta_b}\Big{)}^{2N}\frac{1}{\frac{\pi(n_a+n_b)}{(\frac{n_b\cos\theta_b}{n_a\cos\theta_a}-\frac{n_a\cos\theta_a}{n_b\cos\theta_b})\omega_0n_0\cos\theta_0}+\frac{2\pi}{\omega_0}}
\end{equation}
\begin{equation}
V^2=\frac{1+r_{TE}}{r_{TE}}\frac{\Gamma_0\cos\theta_0}{\alpha_{TE}+\frac{2\pi}{\omega_0}}=\frac{2\Gamma_0\cos\theta_0}{\frac{\pi(n_a+n_b)}{(\frac{n_b\cos\theta_b}{n_a\cos\theta_a}-\frac{n_a\cos\theta_a}{n_b\cos\theta_b})\omega_0n_0\cos\theta_0}+\frac{2\pi}{\omega_0}}
\end{equation}
~\\
for TM mode
\begin{equation}
\gamma_c=\frac{1-r_{TM}}{r_{TM}}\frac{1}{\alpha_{TM}+\frac{2\pi}{\omega_0}}=2\frac{n_f\cos\theta_0}{n_0\cos\theta_f}\Big{(}\frac{n_a\cos\theta_b}{n_b\cos\theta_a}\Big{)}^{2N}\frac{1}{\frac{\pi(n_a+n_b)\cos\theta_0}{(\frac{n_b\cos\theta_a}{n_a\cos\theta_b}-\frac{n_a\cos\theta_b}{n_b\cos\theta_a})\omega_0n_0}+\frac{2\pi}{\omega_0}}
\end{equation}
\begin{equation}
V^2=\frac{1+r_{TM}}{r_{TM}}\frac{\Gamma_0/\cos\theta_0}{\alpha_{TM}+\frac{2\pi}{\omega_0}}=\frac{2\Gamma_0/\cos\theta_0}{\frac{\pi(n_a+n_b)\cos\theta_0}{(\frac{n_b\cos\theta_a}{n_a\cos\theta_b}-\frac{n_a\cos\theta_a}{n_b\cos\theta_b})\omega_0n_0}+\frac{2\pi}{\omega_0}}
\end{equation}
~\\
The spectrum dependence on in-plane momentum $q$ is hidden in $\omega_0$ and $\omega_{ex}$:
\begin{equation}
\omega_0=\omega_{0}(q=0)+\frac{q^2}{2m_{cav}} ~~~ \omega_{ex}=\frac{q^2}{2m_{ex}}
\end{equation}
The radiative dependence on in-plane momentum $q$ is expressed in terms of angles:
\begin{equation}
q=\frac{2\pi}{\lambda_0}\theta_0 ~~~ n_0\theta_0=n_a\theta_a=n_b\theta_b
\end{equation}
for TE mode
\begin{equation}
\gamma_c\propto\Big{(}\frac{n_a\cos\theta_a}{n_b\cos\theta_b}\Big{)}^{2N}=\Big{(}\frac{n_a}{n_b}\Big{)}^{2N}\exp\Big{[}-\frac{N}{2}(\frac{n_0^2}{n^2_a}-\frac{n^2_0}{n^2_b})\frac{\lambda^2_0}{4\pi^2}q^2\Big{]}
\end{equation}
for TM mode
\begin{equation}
\gamma_c\propto\Big{(}\frac{n_a\cos\theta_b}{n_b\cos\theta_a}\Big{)}^{2N}=\Big{(}\frac{n_a}{n_b}\Big{)}^{2N}\exp\Big{[}\frac{N}{2}(\frac{n_0^2}{n^2_a}-\frac{n^2_0}{n^2_b})\frac{\lambda^2_0}{4\pi^2}q^2\Big{]}
\end{equation}





\end{document}